
typedef struct
{
	char cmd;						// command
	char str[30];				// command description
	void (*op)();			// command operation
	void* subMenu;			// submenu
} menuItem;

typedef struct
{
	char str[50];			// header text
	menuItem* item;		// menu item
	uint16_t		cnt;	// menu item count
} menuTable;


menuItem mainMenuItem[] = {
	//	cmd		str						op			subMenu
	{		'C',	"Copy",				NULL,		NULL},
	{		'V',	"Paste",			NULL,		NULL},
	{		'X',	"Cut",				NULL,		NULL},
	{		'Z',	"Undo",				NULL,		NULL},
	{		'Y',	"Redo",				NULL,		NULL},
};

#define MAIN_MENU_ITEM_COUNT (sizeof(mainMenuItem)/sizeof(menuItem))

menuTable mainMenuTable = {
	"Simple Diagnostics",
	mainMenuItem,
	MAIN_MENU_ITEM_COUNT
};

