typedef struct
{
	char cmd;									// command
	char str[30];							// command description
	uint8_t (*op)(char cmd);	// command operation
	void* subMenu;						// submenu
} menuItem;

typedef struct
{
	char str[50];			// header text
	menuItem* item;		// menu item
	uint16_t		cnt;	// menu item count
} menuTable;

uint16_t searchMenu(menuTable* menu, char key);
uint8_t runMenuItem(menuItem* menu,char cmd);
void runMenu(menuTable* menu);
uint8_t exitMenu(char cmd);

extern menuTable mainMenu;
extern menuItem mainMenuItem[];
extern menuTable accMenu;
extern menuItem accMenuItem[];
extern menuTable gselMenu;
extern menuItem gselMenuItem[];
