
#include "main.h"
#include "lpc177x_8x_i2c.h"

int main(void)
{
	
  TIM_TIMERCFG_Type timerCfg;

  // initialize timer
  TIM_ConfigStructInit(TIM_TIMER_MODE, &timerCfg);
  TIM_Init(LPC_TIM0, TIM_TIMER_MODE, &timerCfg);
	
	console_init();
	input_init();
  i2c0_pinConfig();
  I2C_Init(I2C_0, 100000);
  I2C_Cmd(I2C_0, ENABLE);
  acc_init();
	
	runMenu(&mainMenu);
}
