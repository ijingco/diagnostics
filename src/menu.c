#include "main.h"

// Main diagnostic menu-------------------------------------//
menuItem mainMenuItem[] = {
	//	cmd		str											op							subMenu
	{		'A',	"Accelerometer",				NULL,						&accMenu},
	{		'x',	"None",									NULL,						NULL},
	{		'x',	"None",									NULL,						NULL},
	{		'x',	"None",									NULL,						NULL},
	{		'x',	"None",									NULL,						NULL}
};

#define MAIN_MENU_ITEM_COUNT (sizeof(mainMenuItem)/sizeof(menuItem))

menuTable mainMenu = {
	"Simple Diagnostics",
	mainMenuItem,
	MAIN_MENU_ITEM_COUNT
};
// ---------------------------------------------------------//

// Accelerometer diagnostic menu----------------------------//
menuItem accMenuItem[] = {
	//	cmd		str																op							subMenu
	{		'S',	"Set G-range",										NULL,						&gselMenu},
	{		'R',	"Read value",											readAcc,				NULL},
	{		'X',	"Exit",														exitMenu,				NULL}
};

#define ACC_MENU_ITEM_COUNT (sizeof(accMenuItem)/sizeof(menuItem))

menuTable accMenu = {
	"Accelerometer Diagnostics",
	accMenuItem,
	ACC_MENU_ITEM_COUNT
};
// ---------------------------------------------------------//

// Accelerometer diagnostic menu----------------------------//
menuItem gselMenuItem[] = {
	//	cmd		str												op							subMenu
	{		'A',	"8g",											setGsel,				NULL},
	{		'B',	"4g",											setGsel,				NULL},
	{		'C',	"2g",											setGsel,				NULL},
	{		'X',	"Cancel",									exitMenu,				NULL}
};

#define GSEL_MENU_ITEM_COUNT (sizeof(gselMenuItem)/sizeof(menuItem))

menuTable gselMenu = {
	"Please sellect acceleration measurement range",
	gselMenuItem,
	GSEL_MENU_ITEM_COUNT
};
// ---------------------------------------------------------//

uint16_t searchMenu(menuTable* menu, char key)
{
	uint16_t c = 0;
	menuItem* items = menu->item;
	
	while(c < menu->cnt)	
	{
		if((items[c].cmd == key)
		||(items[c].cmd+0x20 == key))
			return c;
		c++;
	}
	return c;
}

void runMenu(menuTable* menu)
{
	uint8_t running = 1;
	uint16_t c;
	uint16_t index;
	char input;
	menuItem* items = menu->item;
	
	
	while(running)
	{
		printString(menu->str);
		
		for(c = 0;c < menu->cnt; c++)
			printMenuItem(items[c].cmd,items[c].str);
		
		printString("Please sellect a command");
		
		input = readInput();
		printString("");
		printString("");
		index = searchMenu(menu, input);
		
		if(index < menu->cnt)
			running = runMenuItem(&items[index],input);
	}
	
	//running = 0;
}

uint8_t runMenuItem(menuItem* menu,char cmd)
{
	if(menu->subMenu != NULL)
		runMenu(menu->subMenu);
	else if(menu->op != NULL)
		return menu->op(cmd);
	return 1;
}


uint8_t exitMenu(char cmd)
{
	return 0;
}
