
#include <stdio.h>
#include "main.h"
#include "lpc177X_8X_uart.h"

void printMenuItem(char cmd,char str[30])
{
  char buff[40];
	sprintf(buff, "    %c - %s\r\n", cmd, str);
	console_sendString((uint8_t*)buff);
}

void printString(char str[50])
{
  char buff[60];
	sprintf(buff, "%s\r\n", str);
	console_sendString((uint8_t*)buff);
}

uint8_t rev_buf[100];          // Reception buffer
uint32_t rev_cnt = 0;          // Reception counter
uint32_t old_cnt = 0;          // Reception counter

void input_init()
{
	// UART FIFO configuration Struct variable
	UART_FIFO_CFG_Type UARTFIFOConfigStruct;

	UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);

	// Initialize FIFO for UART0 peripheral
	UART_FIFOConfig(LPC_UART0, &UARTFIFOConfigStruct);
	UART_IntConfig(LPC_UART0, 0,ENABLE);
}


char readInput()
{
	NVIC_EnableIRQ(UART0_IRQn);
	
	while(rev_cnt == old_cnt)
	{
		//wait for input
	}
	old_cnt = rev_cnt;
	return (char)(rev_buf[rev_cnt-1]);
}

void UART0_IRQHandler(void)
{
    /* Read the received data */
    if(UART_Receive(LPC_UART0, &rev_buf[rev_cnt], 1, NONE_BLOCKING) == 1) {

        // Echo characters received
        UART_Send(LPC_UART0, &rev_buf[rev_cnt], 1, NONE_BLOCKING);
        rev_cnt++;
        if(rev_cnt == 100) rev_cnt = 0;
				NVIC_DisableIRQ(UART0_IRQn);
    }
}
