#include "main.h"
#include "acc.h"
#include "lpc177x_8x_i2c.h"

#define ACC_MCTL_MODE(m) ((m) << 0)
#define ACC_MCTL_GLVL(g) ((g) << 2)

#define I2C_PORT (I2C_0)

#define ACC_I2C_ADDR    (0x1D)
#define ACC_ADDR_MCTL   0x16

static Status I2CWrite(uint32_t addr, uint8_t* buf, uint32_t len) 
{
  I2C_M_SETUP_Type i2cData;

	i2cData.sl_addr7bit = addr;
	i2cData.tx_data = buf;
	i2cData.tx_length = len;
	i2cData.rx_data = NULL;
	i2cData.rx_length = 0;
	i2cData.retransmissions_max = 3;

  return I2C_MasterTransferData(I2C_PORT, &i2cData, I2C_TRANSFER_POLLING);
}

static Status I2CRead(uint32_t addr, uint8_t* buf, uint32_t len) 
{
  I2C_M_SETUP_Type i2cData;

	i2cData.sl_addr7bit = addr;
	i2cData.tx_data = NULL;
	i2cData.tx_length = 0;
	i2cData.rx_data = buf;
	i2cData.rx_length = len;
	i2cData.retransmissions_max = 3;

  return I2C_MasterTransferData(I2C_PORT, &i2cData, I2C_TRANSFER_POLLING);
}

void setModeCtl(uint8_t mctl)
{
    uint8_t buf[2];

		buf[0] = ACC_ADDR_MCTL;
    buf[1] = mctl;
    I2CWrite(ACC_I2C_ADDR, buf, 2);
}

uint8_t setGsel(char cmd)
{
	uint8_t range;
	
	switch (cmd)
	{
		case 'A':
		case 'a':
			range = ACC_MCTL_GLVL(ACC_RANGE_8G);
			break;
		
		case 'B':
		case 'b':
			range = ACC_MCTL_GLVL(ACC_RANGE_4G);
			break;
		
		case 'C':
		case 'c':
			range = ACC_MCTL_GLVL(ACC_RANGE_2G);
			break;
	}
	setModeCtl( (ACC_MCTL_MODE(ACC_MODE_MEASURE) | range ));
	printString("Range was set");
	printString("");
	return 0;
}

uint8_t readAcc(char cmd)
{
  char buff[60];
  int32_t xoff;
  int32_t yoff;
  int32_t zoff;

  int8_t x;
  int8_t y;
  int8_t z;

  acc_read(&x, &y, &z);
  xoff = 0-x;
  yoff = 0-y;
  zoff = 64-z;

	acc_read(&x, &y, &z);
	x = x+xoff;
	y = y+yoff;
	z = z+zoff;

  sprintf(buff, "x=%d, y=%d, z=%d\r\n", (int)x, (int)y, (int)z);
	printString(buff);
	return 1;
}
